#! /usr/bin/env python

# Andrew Lampert
# 250 643 797
# ALamper@uwo.ca

# IMPORTS
import rsa
import pkcs
import filemanager

# VARIABLES
filepath 			= "sign.py"
filepath_signature 	= "signature.txt"
filepath_given_data 	= "assignment2-values.txt"


try:

	file_content 	= filemanager.read_file(filepath)

	m	 			= pkcs.padding(file_content, 256)

	d 				= filemanager.get_file_var(filepath_given_data, "d")

	n 				= filemanager.get_file_var(filepath_given_data, "n")

	signature 		= pow( int(m, 16), int(d), int(n))

	print signature

	filemanager.write_file(filepath_signature, str(signature))



except Exception as error:
	print repr(error);