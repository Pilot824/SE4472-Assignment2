#! /usr/bin/env python

# Andrew Lampert
# 250 643 797
# ALamper@uwo.ca

def read_file(file_path):
	file = open(file_path, "r")
	file_content = file.read()
	file.close()
	return file_content

def get_file_var(file_path, var):

	var_val = ""

	lines = [line.rstrip('\n') for line in open(file_path)]

	for line in lines:
		if line[:4] == (var + " = "):
			var_val = line[4:]
			
	return var_val

def write_file(file_path, content):
	file = open(file_path, "w")
	file_content = file.write(content)
	file.close()